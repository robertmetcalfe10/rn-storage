package com.rn_storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class SharedPreferencesModule extends ReactContextBaseJavaModule {

    public SharedPreferencesModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "SharedPreferences";
    }

    @ReactMethod
    public void save(String key, String value, Callback successCallback, Callback errorCallback) {
        try {
            Log.v("SP save", String.valueOf(System.currentTimeMillis()));
            SharedPreferences sharedPref = this.getCurrentActivity().getSharedPreferences("REACT_NATIVE", Context.MODE_PRIVATE);
            sharedPref.edit().putString(key,value).apply();
            Log.v("SP save", String.valueOf(System.currentTimeMillis()));
            successCallback.invoke("Saved to SharedPreferences");
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }

    @ReactMethod
    public void get(String key, Callback successCallback, Callback errorCallback) {
        try {
            Log.v("SP get", String.valueOf(System.currentTimeMillis()));
            SharedPreferences sharedPref = this.getCurrentActivity().getSharedPreferences("REACT_NATIVE", Context.MODE_PRIVATE);
            String result = sharedPref.getString(key,"Does Not Exist");
            Log.v("SP get", String.valueOf(System.currentTimeMillis()));
            if (result.equals("Does Not Exist")) {
                throw new Exception("Value does not exist");
            } else {
                successCallback.invoke(result);
            }
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }
}
