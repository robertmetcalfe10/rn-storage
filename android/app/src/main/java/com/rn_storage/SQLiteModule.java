package com.rn_storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class SQLiteModule extends ReactContextBaseJavaModule {

    private static AppDatabase sqlDB;

    public SQLiteModule(ReactApplicationContext reactContext) {
        super(reactContext);
        sqlDB = AppDatabase.getAppDatabase(reactContext);
    }

    @Override
    public String getName() {
        return "SQLite";
    }

    @ReactMethod
    public void saveUser(String fName, String lName, int age, Callback successCallback, Callback errorCallback) {
        try {
            User user = new User();
            user.setFirstName(fName);
            user.setLastName(lName);
            user.setAge(age);
            Log.v("SQL save", String.valueOf(System.currentTimeMillis()));
            sqlDB.userDao().insertAll(user);
            Log.v("SQL save", String.valueOf(System.currentTimeMillis()));
            successCallback.invoke("Saved user to SQL");
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }

    @ReactMethod
    public void getUser(String fName, String lName, Callback successCallback, Callback errorCallback) {
        try {
            Log.v("SQL get", String.valueOf(System.currentTimeMillis()));
            User user = sqlDB.userDao().findByName(fName,lName);
            Log.v("SQL get", String.valueOf(System.currentTimeMillis()));
            successCallback.invoke("User: "+user.getFirstName()+" "+user.getLastName()+" "+user.getAge());
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }
}