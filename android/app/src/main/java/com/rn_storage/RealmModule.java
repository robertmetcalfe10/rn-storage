package com.rn_storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import io.realm.Realm;

import static com.facebook.react.bridge.UiThreadUtil.runOnUiThread;

public class RealmModule extends ReactContextBaseJavaModule {

    private static Realm realm;

    public RealmModule(ReactApplicationContext reactContext) {
        super(reactContext);
        Realm.init(reactContext);
    }

    @Override
    public String getName() {
        return "Realm";
    }

    @ReactMethod
    public void saveUser(String fName, String lName, int age, Callback successCallback, Callback errorCallback) {
        try {
            realm = Realm.getDefaultInstance();
            final UserRealm user = new UserRealm();
            user.setFirstName(fName);
            user.setLastName(lName);
            user.setAge(age);
            Log.v("Realm save", String.valueOf(System.currentTimeMillis()));
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(user);
            realm.commitTransaction();
            Log.v("Realm save", String.valueOf(System.currentTimeMillis()));
            successCallback.invoke("Saved to Realm");
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }

    @ReactMethod
    public void getUser(String fName, String lName, Callback successCallback, Callback errorCallback) {
        try {
            Log.v("Realm get", String.valueOf(System.currentTimeMillis()));
            UserRealm user = realm.where(UserRealm.class).equalTo("firstName", fName).and().equalTo("lastName",lName).findFirst();
            Log.v("Realm get", String.valueOf(System.currentTimeMillis()));
            successCallback.invoke("User: "+user.getFirstName()+" "+user.getLastName()+" "+user.getAge());
        } catch(Exception e) {
            errorCallback.invoke(e);
        }
    }
}
