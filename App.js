import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import {Button} from 'react-native-elements';
import {NativeModules} from 'react-native';

var SharedPreferences = NativeModules.SharedPreferences;
var SQLite = NativeModules.SQLite;
var Realm = NativeModules.Realm;

export default class App extends Component<Props> {

  saveSP() {
    console.log(Date.now());
    SharedPreferences.save("Test", "Rob was here", (err) => {
      console.log(err);
      console.log(Date.now());
    }, (msg) => {
      console.log(msg);
      console.log(Date.now());
    })
  }

  getSP() {
    console.log(Date.now());
    SharedPreferences.get("Test", (err) => {
      console.log(err);
      console.log(Date.now());
    }, (msg) => {
      console.log(msg);
      console.log(Date.now());
    })
  }

  saveSQL() {
    console.log(Date.now());
    SQLite.saveUser("Robert", "Metcalfe", 23, (msg) => {
        console.log(msg);
        console.log(Date.now());
    }, (err) => {
        console.log(err);
        console.log(Date.now());
    })
  }

  getSQL() {
    console.log(Date.now());
    SQLite.getUser("Robert", "Metcalfe", (msg) => {
      console.log(msg);
      console.log(Date.now());
    }, (err) => {
      console.log(err);
      console.log(Date.now());
    })
  }

  saveRealm() {
    console.log(Date.now());
    Realm.saveUser("Robert", "Metcalfe", 21, (msg) => {
        console.log(msg);
        console.log(Date.now());
    }, (err) => {
        console.log(err);
        console.log(Date.now());
    })
  }

  getRealm() {
    console.log(Date.now());
    Realm.getUser("Robert", "Metcalfe", (msg) => {
        console.log(msg);
        console.log(Date.now());
    }, (err) => {
        console.log(err);
        console.log(Date.now());
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title={"Save to shared preferences"} onPress={this.saveSP} buttonStyle={{margin: 5}}/>
        <Button title={"Get from shared preferences"} onPress={this.getSP} buttonStyle={{margin: 5}}/>
        <Button title={"Save to SQLite"} onPress={this.saveSQL} buttonStyle={{margin: 5}}/>
        <Button title={"Get from SQLite"} onPress={this.getSQL} buttonStyle={{margin: 5}}/>
        <Button title={"Save to Realm"} onPress={this.saveRealm} buttonStyle={{margin: 5}}/>
        <Button title={"Get from Realm"} onPress={this.getRealm} buttonStyle={{margin: 5}}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
